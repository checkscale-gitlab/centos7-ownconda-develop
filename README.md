Centos7 with ownconda build tools
=================================

This is a minimal Centos7 image with our ownconda Python distribution,
[conda-build](https://github.com/conda/conda-build) and [ownconda
tools](https://git.services.own/ownconda/ownconda-tools).


## Run the container

```bash
$ docker run --rm -it -v ~/Projects:/projects forge.services.own/centos7-ownconda-develop bash
```


## Build an run the container locally

You can build and run the container locally:

```bash
$ docker build -t centos7-ownconda-develop .
$ docker run --rm -it -v ~/Projects:/projects centos7-ownconda-develop bash
```

See `docker build --help` or `docker run --help` for details.
